local mod	= DBM:NewMod("Fathomlord", "DBM-Serpentshrine")
local L		= mod:GetLocalizedStrings()

mod:SetRevision(("$Revision: 163 $"):sub(12, -3))

mod:SetCreatureID(21214)
mod:RegisterCombat("yell", L.YellPull)

mod:RegisterEvents(
	"SPELL_CAST_START",
    "SPELL_CAST_SUCCESS"
)

local timerNovaCD        = mod:NewCDTimer(30, 38445)
local timerSpitfireCD    = mod:NewCDTimer(60, 38236)

local berserkTimer          = mod:NewBerserkTimer(600)

function mod:OnCombatStart()
    berserkTimer:Start()
    timerNovaCD:Start()
    timerSpitfireCD:Start()
end

function mod:SPELL_CAST_START(args)
    if args:IsSpellID(38445) then
        timerNovaCD:Start()
    end
end

function mod:SPELL_CAST_SUCCESS(args)
    if args:IsSpellID(38236) then
        timerSpitfireCD:Start()
    end
end
